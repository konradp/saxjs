function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}

window.onload = function() {
  let notes = SAX_NOTES;
  const div = _getId('app');
  for (let note in notes) {
    const canvasId = 'canvas' + note;
    if (note%6 == 0) {
      // Insert break every four notes
      let br = _new('br');
      div.appendChild(br);
    }
    let canvas = _new('canvas');
    canvas.id = canvasId;
    canvas.style = 'border:1px solid;width:15%';
    div.appendChild(canvas);
    let sax = new Sax(canvasId);
    sax.setFingering(notes[note]);
  }
}
