
window.onload = function() {
  const sax = new TestDraw('test_draw');
}
function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}

class TestDraw {
  constructor(canvas_id) {
    this.canvas = _getId(canvas_id);
    this.context = this.canvas.getContext('2d');
    this.key_size = 14;
    this.img = new Image();
    this.img.src = '/old_examples/example2.png';
    this.canvas.width = window.innerWidth/2;
    this.canvas.height = window.innerHeight - 20;
    this.canvas.width = this.img.width;
    this.canvas.height = this.img.height;
    this.img.onload = () => {
      this.DrawSax();
    };
    this.context.lineWidth = 4;
    this.context.strokeStyle = 'red';
    this.controls = {
      octave: {
        x: 2*this.key_size,
        y: 48,
        radX: 8,
        radY: 16,
        rot: 0,
      },
      highFs: {
        x: 63,
        y: 280,
        w: 24,
        h: 50,
        A: 10,
        B: 20, // TODO: can it be 0?
        C: 4,
      },
      fsAlternate: {
        x: 60,
        y: 322,
        radX: 8,
        radY: 16,
        rot: 13,
      },
      frontF: {
        x: 76,
        y: 0,
        w: 24,
        h: 50,
      },
    };
    this.DrawControls();
  }

  DrawControls() {
    const controls = this.controls;
    const div = _getId('controls');
    for (let key in controls) {
      let title = _new('strong');
      title.innerHTML = key;
      div.appendChild(title);
      div.appendChild(_new('br'));
      for (let attr in controls[key]) {
        let l = _new('label');
        l.innerHTML = attr + ':';
        let i = _new('input');
        i.setAttribute('type', 'number');
        i.setAttribute('value', controls[key][attr]);
        i.dataset['key'] = key;
        i.dataset['attr'] = attr;
        i.addEventListener('input', this.onControlUpdate.bind(this));
        div.appendChild(l);
        div.appendChild(i);
        div.appendChild(_new('br'));
      }
    }
  } // DrawControls

  DrawSax() {
    // Draw high F#
    const c = this.context;
    c.clearRect(0, 0, this.canvas.width, this.canvas.height);
    //c.drawImage(this.img, 0, 0);
    // Left hand
    this.DrawOctave(50, 50, 50, 80);
    this.DrawPalmKeys();
    this.DrawCircle(78, 70,  this.key_size); // 1
    this.DrawCircle(80, 127, this.key_size); // 2
    this.DrawCircle(98, 156,  this.key_size); // 3
    this.DrawFrontF(50, 50, 50, 80); // above 1
    this.DrawCircle(76, 99,  8); // Bb key: between 1 and 2
    this.DrawPinkyLH(154, 178);
    // Right hand
    // RH 1,2,3
    this.DrawCircle(98, 254, this.key_size); // 1
    this.DrawCircle(104, 307, this.key_size); // 2
    this.DrawCircle(103, 357, this.key_size); // 3
    this.DrawFsAlternate(50, 50, 50, 80); // between 2 and 3
    this.DrawSideKeys(2*this.key_size, 182, 10, 100);
    this.DrawHighFSharp(50, 50, 50, 80); // between 1 and 2
    this.DrawPinkyRH(54, 404);
  }; // DrawSax()


  DrawPinkyLH(x, y) {
    // TODO: use coords here that will scale
    const c = this.context;
    this.DrawCircle(x, y, 30);
    c.beginPath();
    c.moveTo(x-30+30/6, y-30+30/3);
    c.bezierCurveTo(
      x-10, y, // cp1
      x+10, y, // cp2
      x+30-30/6, y-30+30/3, // end
    );
    c.stroke();

    c.beginPath();
    c.moveTo(x-30+30/6, y+30-30/3);
    c.bezierCurveTo(
      x-10, y, // cp1
      x+10, y, // cp2
      x+30-30/6, y+30-30/3, // end
    );
    c.stroke();
  }


  DrawSideKeys(x, y, w, h) {
    const c = this.context;
    const h_divider = h/18;
    const h_height = (h - 2*h_divider)/3
    c.strokeRect(x-w/2, y, w, h_height);
    c.strokeRect(x-w/2, y+h_height+h_divider, w, h_height);
    c.strokeRect(x-w/2, y+2*h_height+2*h_divider, w, h_height);
  }


  DrawPinkyRH(x, y) {
    // TODO: use coords here that will scale
    const c = this.context;
    this.DrawCircle(x, y, 28);
    c.beginPath();
    c.moveTo(x-30+30/6, y);
    c.lineTo(x+30-30/6, y);
    c.stroke();
  }


  DrawCircle(x, y, rad) {
    const c = this.context;
    c.beginPath();
    c.arc(x, y, rad, 0, 2*Math.PI);
    c.stroke();
  }


  DrawPalmKeys() {
    this.DrawPalmKey(130, 26);
    this.DrawPalmKey(155, 60);
    this.DrawPalmKey(118, 75);
  }

  DrawPalmKey(x, y) {
    // Cubic Bézier curve
    const c = this.context;
    c.beginPath();
    c.moveTo(x, y);
    c.bezierCurveTo(
      x-20, y+54, // cp1
      x+30, y+64, // cp2
      x, y,       // end
    );
    c.stroke();
  }


  DrawOctave(x, y, radX, radY, rot) {
    x = parseInt(document.querySelector("input[data-key='octave'][data-attr='x']").value);
    y = parseInt(document.querySelector("input[data-key='octave'][data-attr='y']").value);
    radX = parseInt(document.querySelector("input[data-key='octave'][data-attr='radX']").value);
    radY = parseInt(document.querySelector("input[data-key='octave'][data-attr='radY']").value);
    rot = parseInt(document.querySelector("input[data-key='octave'][data-attr='rot']").value);
    const c = this.context;
    c.beginPath();
    c.ellipse(x, y, radX, radY, rot*Math.PI/20,
        0, 2*Math.PI);
    c.stroke();
  } // DrawOctave

  DrawFsAlternate(x, y, radX, radY, rot) {
    x = parseInt(document.querySelector("input[data-key='fsAlternate'][data-attr='x']").value);
    y = parseInt(document.querySelector("input[data-key='fsAlternate'][data-attr='y']").value);
    radX = parseInt(document.querySelector("input[data-key='fsAlternate'][data-attr='radX']").value);
    radY = parseInt(document.querySelector("input[data-key='fsAlternate'][data-attr='radY']").value);
    rot = parseInt(document.querySelector("input[data-key='fsAlternate'][data-attr='rot']").value);
    const c = this.context;
    c.beginPath();
    c.ellipse(x, y, radX, radY, rot*Math.PI/20,
        0, 2*Math.PI);
    c.stroke();
  } // DrawFsAlternate


  DrawFrontF(x, y, w=50, h=80) {
    // TODO: make scalable
    x = parseInt(document.querySelector("input[data-key='frontF'][data-attr='x']").value);
    y = parseInt(document.querySelector("input[data-key='frontF'][data-attr='y']").value);
    w = parseInt(document.querySelector("input[data-key='frontF'][data-attr='w']").value);
    h = parseInt(document.querySelector("input[data-key='frontF'][data-attr='h']").value);
    const c = this.context;
    c.beginPath();
    c.moveTo(x, y);
    c.lineTo(x, y + h);
    c.bezierCurveTo(
      x-10, y+h+h/8, // cp1
      x-15, y+h+h/8, // cp2
      x-w,  y+h-h/8, // end
    );
    c.lineTo(x, y);
    c.stroke();
  } // DrawFrontF

  DrawHighFSharp(x, y, w=50, h=80) {
    x = parseInt(document.querySelector("input[data-key='highFs'][data-attr='x']").value);
    y = parseInt(document.querySelector("input[data-key='highFs'][data-attr='y']").value);
    w = parseInt(document.querySelector("input[data-key='highFs'][data-attr='w']").value);
    h = parseInt(document.querySelector("input[data-key='highFs'][data-attr='h']").value);
    const val_a = parseInt(document.querySelector("input[data-key='highFs'][data-attr='A']").value);
    const val_b = parseInt(document.querySelector("input[data-key='highFs'][data-attr='B']").value);
    const val_c = parseInt(document.querySelector("input[data-key='highFs'][data-attr='C']").value);
    const c = this.context;
    const A = h/val_a;
    const B = w/val_b;
    const C = w/val_c;
    c.beginPath();
    c.moveTo(x + w/2,     y);           // 1
    c.lineTo(x - B, y + h/2);     // 2
    c.lineTo(x - w/2,     y + h/2 - A); // 3
    c.lineTo(x - w/2 + C, y);           // 4
    c.lineTo(x - w/2,     y - h/2 + A); // 5
    c.lineTo(x - B, y - h/2);     // 6
    c.lineTo(x + w/2,     y);           // 1
    c.stroke();
  } // DrawHighFSharp

  onControlUpdate(event) {
    this.DrawSax();
  }

}; // class Sax
