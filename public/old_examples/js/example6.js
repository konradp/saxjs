window.onload = function() {
  const sax = new Sax('canvas', {
    isSimplified: true,
    isStrokeAll: true,
  });

  sax.addCanvasEventListener('mousedown', function(e) {
    // Open or close pads on click
    const c = this.context;
    const state = this.state;
    // Mouse position relative to canvas
    var rect = this.canvas.getBoundingClientRect();
    const mouseX = e.clientX - rect.left;
    const mouseY = e.clientY - rect.top;
    for (i in state) {
      if (state[i].hasOwnProperty('path')) {
        if (c.isPointInPath(state[i].path, mouseX, mouseY)) {
          state[i].isDown = !state[i].isDown;
          this.DrawSax();
        }
      }
    }
  });
}
