function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}
const noteNames = [
    'C', 'C#', 'D', 'D#', 'E', 'F',
    'F#', 'G', 'G#', 'A', 'A#', 'B'
];
const COUNT_SAX_NOTES = 32;
const INTERVALS = {
  maj: [ 0, 2, 4, 5, 7, 9, 11 ],
  min: [ 0, 2, 3, 5, 7, 8, 10 ],
};
let ALL_NOTES = [];

window.onload = function() {
  drawPicker();
  ALL_NOTES = saxNoteNames();
  drawCards();
}

function drawCards() {
  // Clear
  const div = _getId('app');
  div.innerHTML = '';

  // Get scale key and scale type from inputs
  let scale_key = _getId('select_key_note').value;
  let scale_type = _getId('select_key_type').value;

  // Draw
  let indexes = getScaleIndexes(scale_key, scale_type);
  for (let idx of indexes) {
    const canvasId = 'canvas' + idx;
    let canvas = _new('canvas');
    canvas.id = canvasId;
    canvas.style = 'border:1px solid;width:5%';
    div.appendChild(canvas);
    let sax = new Sax(canvasId);
    sax.setFingering(SAX_NOTES[idx]);
    sax.setLabel(ALL_NOTES[idx]);
  }
}

function getScaleIndexes(keyNote, scaleType) {
  let shift = noteNames.indexOf(keyNote) + 2;
  let intervals = (scaleType == 'maj')? INTERVALS.maj : INTERVALS.min;
  // shift intervals for the given keyNote
  let tmp = [];
  for (let i = 0; i<intervals.length; i++) {
    tmp.push((intervals[i]+shift)%12);
  }
  intervals = tmp;
  indexes = [];
  for (let i = 0; i<=COUNT_SAX_NOTES-1; i++) {
    if (intervals.includes(i%12)) {
      indexes.push(i);
    }
  }
  return indexes;
}

function getNoteAt(i) {
  // return a note name:
  // 0=A#, 1=B, 2=C, ..., 12=A#,
  // 13=B, 14=C
  return noteNames[(i+10)%12];
}

function saxNoteNames() {
  // return names of all sax notes:
  // A#, B, C, ..., F
  let notes = [];
  for (let i=0; i<=COUNT_SAX_NOTES-1; i++) {
    notes.push(getNoteAt(i));
  }
  return notes;
}

function drawPicker() {
  // GUI picker:
  // - key note (C, C#, ...)
  // - type: maj/min
  const div = _getId('controls');
  let s = _new('select');
  div.appendChild(s);
  s.id = 'select_key_note';
  for (const note of noteNames) {
    let opt = _new('option');
    opt.innerHTML = note;
    s.appendChild(opt);
  }
  let sType = _new('select');
  div.appendChild(sType);
  sType.id = 'select_key_type';
  for (const type of ['maj', 'min']) {
    let opt = _new('option');
    opt.innerHTML = type;
    sType.appendChild(opt);
  }
  s.addEventListener('change', drawCards);
  sType.addEventListener('change', drawCards);
}
