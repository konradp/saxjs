// Example: Highlight all keys
let fingering = [];
let idx = 0;
window.onload = function() {
  const sax = new Sax('canvas', {
    isSimplified: true,
    strokeAll: true,
  });

  setInterval(function() {
    fingering = [];
    for (let i = 0; i<23; i++) {
      fingering.push(false);
    }
    fingering[idx] = true;
    sax.setFingering(fingering);
    idx++;
    if (idx == 23) idx = 0;
  }, 200);
}
