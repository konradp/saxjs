window.onload = function() {
  //const sax = new Sax('canvas', {
  //  isSimplified: true,
  //  isStrokeAll: false,
  //});
  const sax = new Sax('canvas');
  sax.setFingering([
    [
      true,  false, true , false, true,
      false, false, false, false, false,
      false, false, false, false, false,
      false, false, false, true,  false,
      false, false, false,
    ], [
      true,  false, true,  false, false,
      false, false, false, false, false,
      false, false, false, true,  false,
      false, false, false, false, false,
      false, false, false,
    ], [
      true,  false, true,  true,  false,
      false, false, false, false, false,
      false, false, false, false, false,
      false, false, false, false, false,
      false, false, false,
    ],
  ]);
}
