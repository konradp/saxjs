// Arc test
let coords = {
  x: 100,
  y: 100,
  rad: 30,
  A: 1,
  B: 4,
  C: 5,
  D: 4,
  is_counterclockwise: 0,
}

window.onload = function() {
  const sax = new TestDraw('test_draw');
  //// DEBUG refresh
  //setInterval(function() {
  //    location.reload();
  //}, 1000);
  //// DEBUG
}

function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}

class TestDraw {
  constructor(canvas_id) {
    this.canvas = _getId(canvas_id);
    this.context = this.canvas.getContext('2d');
    this.canvas.width = 200;
    this.canvas.height = 200;
    this.test = 'a';
    this.DrawControls();
    this.Draw();
  }

  DrawControls() {
    let div = _getId('controls');
    for (let field in coords) {
      let label = _new('span');
      label.innerHTML = field + ':';
      let input = _new('input');
      input.name = field;
      input.type = 'number';
      input.value = coords[field];
      div.appendChild(label);
      div.appendChild(input);
      div.appendChild(_new('br'));
      input.addEventListener('input', (e) => {
        coords[e.target.name] = e.target.value;
        this.Draw();
      });
    }
  }

  Draw() {
    const c = this.context;
    const co = coords;
    if (co.is_counterclockwise == 0) {
      co.is_counterclockwise = false;
    } else {
      co.is_counterclockwise = true;
    }
    c.beginPath();
    c.clearRect(0, 0, this.canvas.width, this.canvas.height);
    c.lineWidth = 4;
    c.arc(
      co.x, // x
      co.y, // y
      co.rad, // rad
      co.A*Math.PI/co.B, // start
      co.C*Math.PI/co.D, // end
      co.is_counterclockwise, // counterclockwise
    );
    c.fill();
  }

}; // class Sax
