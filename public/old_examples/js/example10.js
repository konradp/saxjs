function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}

window.onload = function() {
  const notes = [
    [
      true,  false, true , false, true,
      false, false, false, false, false,
      false, false, false, false, false,
      false, false, false, true,  false,
      false, false, false,
    ], [
      true,  false, true,  false, false,
      false, false, false, false, false,
      false, false, false, true,  false,
      false, false, false, false, false,
      false, false, false,
    ], [
      true,  false, true,  false, false,
      false, false, false, false, false,
      false, false, false, true,  false,
      false, false, false, false, false,
      false, false, false,
    ], [
      true,  false, true,  false, false,
      false, false, false, false, false,
      false, false, false, true,  false,
      false, false, false, false, false,
      false, false, false,
    ], [
      true,  false, true,  true,  false,
      false, false, false, false, false,
      false, false, false, false, false,
      false, false, false, false, false,
      false, false, false,
    ],
  ];

  for (let note in notes) {
    const canvasId = 'canvas' + note;
    const div = _getId('app');
    let canvas = _new('canvas');
    canvas.id = canvasId;
    canvas.style = 'border:1px solid; width:10%';
    div.appendChild(canvas);
    let sax = new Sax(canvasId);
    sax.setFingering(notes[note]);
  }
}
