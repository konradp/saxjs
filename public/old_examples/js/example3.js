window.onload = function() {
  const sax = new TestDraw('test_draw');
}
function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}


class TestDraw {
  constructor(canvas_id) {
    this.canvas = _getId(canvas_id);
    this.context = this.canvas.getContext('2d');
    //this.canvas.width = window.innerWidth/2;
    //this.canvas.height = window.innerHeight - 20;
    this.canvas.width = 187;
    this.canvas.height = 435;
    this.context.lineWidth = 4;
    this.context.strokeStyle = 'black';
    this.context.fillStyle = 'black';
    this.unitX = this.canvas.width/100;
    this.unitY = this.canvas.height/100;
    this.key_size = 7*this.unitX;
    this.key_coordinates = {
      LH: {
        octave: { x: 16, y: 22 },
        frontF: { x: 40, y: 0 },
        1: { x: 41, y: 16 },
        2: { x: 42, y: 28 },
        3: { x: 52, y: 34 },
        palm: {
          1: { x: 69, y: 5 },
          2: { x: 82, y: 13 },
          3: { x: 63, y: 17 },
        },
        Bb: { x: 40, y: 22 },
        pinky: { x: 80, y: 42 },
      },
      RH: {
        1: { x: 52, y: 58 },
        2: { x: 55, y: 70 },
        3: { x: 55, y: 82 },
        side: { x: 16, y: 48 },
        fsAlternate: { x: 32, y: 75 },
        highFs: { x: 33, y: 64 },
        pinky: { x: 16, y: 93 },
      },
    };
    this.DrawSax();
  }


  DrawSax() {
    const c = this.context;
    c.clearRect(0, 0, this.canvas.width, this.canvas.height);
    const size = this.key_size;
    // Left hand
    let coords = this.key_coordinates.LH;
    this.DrawOctave(coords.octave, size);
    this.DrawPalmKeys(coords.palm);
    this.DrawFrontF(coords.frontF, size);      // above 1
    this.DrawCircle(coords['1'], size);        // 1
    this.DrawCircle(coords.Bb, (60/100)*size); // Bb key: between 1 and 2
    this.DrawCircle(coords['2'], size);        // 2
    this.DrawCircle(coords['3'], size);        // 3
    this.DrawPinkyLH(coords.pinky, (7/2)*size);

    // Right hand
    coords = this.key_coordinates.RH;
    this.DrawCircle(coords['1'], size); // 1
    this.DrawHighFSharp(coords.highFs, size);       // between 1 and 2
    this.DrawCircle(coords['2'], size); // 2
    this.DrawFsAlternate(coords.fsAlternate, size); // between 2 and 3
    this.DrawCircle(coords['3'], size); // 3
    this.DrawSideKeys(coords.side, 8*size);
    this.DrawPinkyRH(coords.pinky, size);
  }; // DrawSax()


  DrawPinkyLH(coords, size) {
    const c = this.context;
    const x = this.Transform(coords).x - size/2;
    const y = this.Transform(coords).y - size/2;
    c.strokeRect(x, y, size, size); // contour

    c.strokeRect(x, y, size, size/3); // top
    c.strokeRect(x, y+2*size/3, size, size/3); // bottom

    c.strokeRect(x, y+size/3, size/2, size/3); // left
    c.strokeRect(x+size/2, y+size/3, size/2, size/3); // right
  }


  //DrawSideKeys(coords, w, h) {
  DrawSideKeys(coords, size) {
    const c = this.context;
    const h_divider = size/18;
    const w = size/10;
    const x = this.Transform(coords).x;
    const y = this.Transform(coords).y;
    const h_height = (size - 2*h_divider)/3
    c.strokeRect(x-w/2, y, w, h_height);
    c.strokeRect(x-w/2, y+h_height+h_divider, w, h_height);
    c.strokeRect(x-w/2, y+2*h_height+2*h_divider, w, h_height);
  }


  DrawPinkyRH(coords, size) {
    // TODO: use coords here that will scale
    const c = this.context;
    this.DrawCircle(coords, size*2);
    const x = this.Transform(coords).x;
    const y = this.Transform(coords).y;
    c.beginPath();
    c.moveTo(x-30+30/6, y);
    c.lineTo(x+30-30/6, y);
    c.stroke();
  }


  DrawCircle(coords, rad) {
    const c = this.context;
    const d = this.Transform(coords);
    c.beginPath();
    c.arc(d.x, d.y, rad, 0, 2*Math.PI);
    c.stroke();
  }


  DrawPalmKeys(coords) {
    this.DrawPalmKey(coords['1']);
    this.DrawPalmKey(coords['2']);
    this.DrawPalmKey(coords['3']);
  }


  DrawPalmKey(coords) {
    // TODO: Remove hardcoded, use size
    // Cubic Bézier curve
    const c = this.context;
    const x = this.Transform(coords).x;
    const y = this.Transform(coords).y;
    c.beginPath();
    c.moveTo(x, y);
    c.bezierCurveTo(
      x-20, y+54, // cp1
      x+30, y+64, // cp2
      x, y,       // end
    );
    c.stroke();
  }


  Transform(coords) {
    let a = {...coords};
    a.x = this.unitX*coords.x;
    a.y = this.unitY*coords.y;
    return a;
  }


  DrawOctave(coords, size) {
    const c = this.context;
    const x = this.Transform(coords).x;
    const y = this.Transform(coords).y;
    c.beginPath();
    c.ellipse(
      x, y,
      (60/100)*size, // radX
      size,          // radY
      0, 0, 2*Math.PI);
    c.stroke();
  }


  DrawFsAlternate(coords, size) {
    const c = this.context;
    const x = this.Transform(coords).x;
    const y = this.Transform(coords).y;
    c.beginPath();
    c.ellipse(x, y,
      (60/100)*size, // radX
      (120/100)*size, // radY
      13*Math.PI/20, // rot
      0, 2*Math.PI);
    c.stroke();
  }


  DrawFrontF(coords, size) {
    const c = this.context;
    let d = this.Transform(coords);
    let w = (150/100)*size;
    let h = (380/100)*size;
    c.beginPath();
    c.moveTo(d.x, d.y);
    c.lineTo(d.x, d.y + h);
    c.lineTo(d.x-w, d.y + h);
    c.lineTo(d.x, d.y);
    //c.fill(); // TODO
    c.stroke();
  } // DrawFrontF


  DrawHighFSharp(coords, size) {
    const c = this.context;
    const x = this.Transform(coords).x;
    const y = this.Transform(coords).y;
    const h = size*4;
    c.beginPath();
    c.moveTo(x + h/4,       y);
    c.lineTo(x - h/20,      y + h/2);
    c.lineTo(x - h/4,       y + h/2 - h/10);
    c.lineTo(x - h/4 + h/8, y);
    c.lineTo(x - h/4,       y - h/2 + h/10);
    c.lineTo(x - h/20,      y - h/2);
    c.lineTo(x + h/4,       y);
    c.stroke();
  } // DrawHighFSharp
}; // class Sax
