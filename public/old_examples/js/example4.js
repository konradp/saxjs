window.onload = function() {
  const sax = new TestDraw('test_draw');
}
class TestDraw {
  constructor(canvas_id) {
    this.canvas = document.getElementById(canvas_id);
    this.context = this.canvas.getContext('2d');

    // Tangential lines
    const c = this.context;
    const x1 = 220;
    const y1 = 20;

    const x2 = 220;
    const y2 = 130;

    const x3 = 130;
    const y3 = 20;
    c.beginPath();
    c.strokeStyle = 'gray';
    c.moveTo(x1, y1);
    c.lineTo(x2, y2);
    c.lineTo(x3, y3);
    c.stroke();

    // Arc
    c.beginPath();
    c.strokeStyle = 'black';
    c.lineWidth = 5;
    c.moveTo(x1, y1);
    c.arcTo(x2,y2, x3,y3, 20);
    c.stroke();

    // Start point
    c.beginPath();
    c.fillStyle = 'blue';
    c.arc(x1, y1, 5, 0, 2 * Math.PI);
    c.fill();

    // Control points
    c.beginPath();
    c.fillStyle = 'red';
    c.arc(x2, y2, 5, 0, 2 * Math.PI); // Control point one
    c.arc(x3, y3, 5, 0, 2 * Math.PI);   // Control point two
    c.fill();
  }
}; // class Sax
