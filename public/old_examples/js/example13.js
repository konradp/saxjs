function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}


window.onload = function() {
  let c = _getId('canvas');
  c.width = 200;
  c.height = 100;
  c.addEventListener('mousemove', function(e) {
    // Mouse position relative to canvas
    var rect = this.getBoundingClientRect();
    const mouseX = e.clientX - rect.left;
    const mouseY = e.clientY - rect.top;
    updateStats('out', {
      'canvas.width': c.width,
      'canvas.height': c.height,
      'canvas.clientWidth': c.clientWidth,
      'canvas.clientHeight': c.clientHeight,
      hr: null,
      X: Math.round( c.width*(e.clientX - rect.left)/c.clientWidth ),
      Y: Math.round( c.height*(e.clientY - rect.top)/c.clientHeight ),
      hr2: null,
      mouseX: Math.round(mouseX),
      mouseY: Math.round(mouseY),
      'event.screenX': e.screenX,
      'event.screenY': e.screenY,
      'event.clientX': e.clientX,
      'event.clientY': e.clientY,
      'event.pageX': e.pageX,
      'event.pageY': e.pageY,
      'event.x': e.x,
      'event.y': e.y,
    });
  });

}

function updateStats(divId, stats) {
  // divId = the id of the div on the right which displays the stats
  // stats = { clientX: 0, clientY: 1, ... }
  let div = _getId(divId);
  div.innerHTML = '';
  for (let stat in stats) {
    if (stats[stat] == null) {
      div.innerHTML += '<hr>';
    } else {
      div.innerHTML += stat + ': ' + stats[stat] + '<br>';
    }
    
  }
}
