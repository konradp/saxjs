window.onload = function() {
  const sax = new TestDraw('test_draw');
}
function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}


class TestDraw {
  constructor(canvas_id) {
    this.canvas = _getId(canvas_id);
    this.context = this.canvas.getContext('2d');
    this.Draw(100, 50, 50);
  }

  Draw(x, y, size) {
    const c = this.context;
    c.lineWidth = 4;
    c.strokeRect(x, y, size, size); // contour

    c.fillStyle = 'red';
    c.fillRect(x, y, size, size/3); // top
    c.fillStyle = 'blue';
    c.fillRect(x, y+2*size/3, size, size/3); // bottom

    c.fillStyle = 'green';
    c.fillRect(x, y+size/3, size/2, size/3); // left
    c.fillStyle = 'yellow';
    c.fillRect(x+size/2, y+size/3, size/2, size/3); // left
  }

}; // class Sax
