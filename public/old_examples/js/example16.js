const canvasId = 'canvas';
function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}

window.onload = function() {
  let notes = SAX_NOTES;
  const div = _getId('app');
  let canvas = _new('canvas');
  canvas.id = canvasId;
  canvas.style = 'border:1px solid;width:15%';
  div.appendChild(canvas);
  let sax = new Sax(canvasId);
  //sax.setFingering(notes[note]);

  // handlers
  _getId('width').addEventListener('input', updateW);
  _getId('height').addEventListener('input', updateH);
}

function updateH(e) {
  console.log('h', e.target.value);
  _getId(canvasId).style.setProperty('width', e.target.value+'%');
}
function updateW(e) {
  console.log('h', e.target.value);
  _getId(canvasId).style.setProperty('height', e.target.value+'%');
}
