window.onload = function() {
  const sax = new Sax('canvas', {
    isSimplified: true,
    isStrokeAll: true,
  });

  sax.addCanvasEventListener('mousemove', function(e) {
    const c = this.context;
    const state = this.state;
    const div = document.getElementById('name');
    // Mouse position relative to canvas
    var rect = this.canvas.getBoundingClientRect();
    const mouseX = e.clientX - rect.left;
    const mouseY = e.clientY - rect.top;
    for (i in state) {
      if (state[i].hasOwnProperty('path')) {
        if (c.isPointInPath(state[i].path, mouseX, mouseY)) {
          div.innerHTML = state[i].longName;
          state[i].isDown = true;
          this.DrawSax();
        } else {
          // Unpress all other keys
          state[i].isDown = false;
        }
      } else {
        div.innerHTML = '';
      }
    }
  });
}
