class Sax {
  constructor(canvas_id, settings={}) {
    // User-settable general settings
    this.settings = {
      isStrokeAll: false, // whether to draw all keys even if not pressed,
      isHidePicker: false, // hide multiple fingerings picker
    };
    this.settings = { ...this.settings, ...settings };
    this.canvas = document.getElementById(canvas_id);
    this.context = this.canvas.getContext('2d');
    this.canvas.width = 140;
    this.canvas.height = 435;
    this.context.lineWidth = 3;
    this.context.strokeStyle = 'black';
    this.context.fillStyle = 'black';
    this.fingering = [];
    this.label = null;
    this.isDrawLabel = false;
    this.unitX = this.canvas.width/100;
    this.unitY = this.canvas.height/100;
    this.coords_data = {
      octave:      { x: 20, y: 10 },
      frontF:      { x: 50, y:  5 },
      lh1:         { x: 50, y: 15 },
      lhBb:        { x: 64, y: 22 },
      lh2:         { x: 50, y: 29 },
      lh3:         { x: 50, y: 43 },
      palm1:       { x: 80, y: 15 },
      palm2:       { x: 92, y: 22 },
      palm3:       { x: 80, y: 29 },
      lhPinky:     { x: 81, y: 50 },
      rh1:         { x: 50, y: 57 },
      rh2:         { x: 50, y: 71 },
      rh3:         { x: 50, y: 85 },
      side:        { x: 19, y: 48 },
      highFs:      { x: 36, y: 64 },
      fsAlternate: { x: 36, y: 78 },
      rhPinky:     { x: 19, y: 94 },
    };
    this.coords = {};
    this.switcher = {
      enabled: false,
      pos: { x: 0, y: 0 },
      idx: 1,
      paths: {
        left: null,
        right: null,
      },
    };
    this.state = [
      {
        id: 0,
        name: 'octave',
        longName: 'octave',
        path: null, // the canvas path
        isDown: false,
        isHighlighted: false,
      },
      { name: 'frontF', longName: 'F front' },
      { name: 'lh1', longName: 'B first' },
      { name: 'lhBb', longName: 'Bb' },
      { name: 'lh2', longName: 'A/C second' },
      { name: 'lh3', longName: 'G third'  },
      { name: 'palm1', longName: 'Eb palm' },
      { name: 'palm2', longName: 'D palm' },
      { name: 'palm3', longName: 'F palm' },
      { name: 'lhPinkyTop', longName: 'G#' },
      { name: 'lhPinkyLeft', longName: 'B low' }, // inner
      { name: 'lhPinkyRight', longName: 'C# low' }, // outer
      { name: 'lhPinkyBottom', longName: 'Bb low' },
      { name: 'rh1', longName: 'F first' },
      { name: 'rh2', longName: 'E second' },
      { name: 'rh3', longName: 'D third' },
      { name: 'side1', longName: 'E side' },
      { name: 'side2', longName: 'C side' },
      { name: 'side3', longName: 'Bb side' },
      { name: 'highFs', longName: 'F# high' },
      { name: 'fsAlternate', longName: 'F# alternate' },
      { name: 'rhPinky1', longName: 'Eb low' },
      { name: 'rhPinky2', longName: 'C low' },
    ];
    this.Init();
    this.DrawSax();
  }; // end constructor


  drawSwitcher() {
    if (!this.switcher.enabled) {
      return;
    }
    const c = this.context;
    // TODO: move pos and size to 'this'
    const pos = this.Transform({x: 75, y: 96});
    const size = this.Transform({x: 55, y: 8});
    const x = pos.x;
    const y = pos.y;
    const sx = size.x;
    const sy = size.y;
    const as = 1*sx/8;

    // right arrow
    let pr = new Path2D;
    pr.moveTo(x+as,   y+sy/2);
    pr.lineTo(x+as,   y-sy/2);
    pr.lineTo(x+sx/2, y);
    pr.lineTo(x+as,   y+sy/2);
    if (this.switcher.idx == this.fingering.length) {
      c.fillStyle = 'grey';
    }
    c.fill(pr);
    c.fillStyle = 'black';
    this.switcher.right = pr;

    // left arrow
    let pl = new Path2D;
    pl.moveTo(x-as,   y+sy/2);
    pl.lineTo(x-as,   y-sy/2);
    pl.lineTo(x-sx/2, y);
    pl.lineTo(x-as,   y+sy/2);
    if (this.switcher.idx == 1) {
      c.fillStyle = 'grey';
    }
    c.fill(pl);
    c.fillStyle = 'black';
    this.switcher.left = pl;

    // Text
    let text = this.switcher.idx;
    c.font = "22px sans bold";
    let t = c.measureText(text);
    c.fillText(text, x-t.width/2, y+t.width/2);
  }


  Init() {
    // Things which require calculation
    this.key_size = 14*this.unitX;
    this.coords = this.coords_data;
    for (let i = 0; i<23; i++) {
      this.fingering.push(false);
      this.state[i].isDown = false;
      this.state[i].isHighlighted = false;
    }
    // Transform coords from 0-100 x 0-100 to actual canvas coordinates
    for (let field in this.coords) {
      this.coords[field] = this.Transform(this.coords[field]);
    }
    this.canvas.addEventListener('mousedown', (e) => {
      // Switcher mouse actions
      if (!this.switcher.enabled) return;
      const c = this.context;
      const ca = this.canvas;
      const state = this.state;
      // Mouse position relative to canvas
      var rect = this.canvas.getBoundingClientRect();
      const mouseX = ca.width*(e.clientX - rect.left)/ca.clientWidth;
      const mouseY = ca.height*(e.clientY - rect.top)/ca.clientHeight;
      if (c.isPointInPath(this.switcher.right, mouseX, mouseY)) {
        if (this.switcher.idx < this.fingering.length) {
          this.switcher.idx++;
          this._setFingering(this.fingering[this.switcher.idx-1]);
        }
      } else if (c.isPointInPath(this.switcher.left, mouseX, mouseY)) {
        if (this.switcher.idx > 1) {
          this.switcher.idx--;
          this._setFingering(this.fingering[this.switcher.idx-1]);
        }
      }
    }); // end mousedown
    this.canvas.addEventListener('mousemove', (e) => {
      // Switcher mouse actions
      if (!this.switcher.enabled) return;
      const c = this.context;
      const ca = this.canvas;
      const state = this.state;
      // Mouse position relative to canvas
      var rect = this.canvas.getBoundingClientRect();
      const mouseX = ca.width*(e.clientX - rect.left)/ca.clientWidth;
      const mouseY = ca.height*(e.clientY - rect.top)/ca.clientHeight;
      if ((c.isPointInPath(this.switcher.right, mouseX, mouseY)
        && this.switcher.idx < this.fingering.length
      ) || (
        c.isPointInPath(this.switcher.left, mouseX, mouseY)
        && this.switcher.idx > 1
      )) {
        ca.style.setProperty('cursor', 'pointer');
      } else {
        ca.style.setProperty('cursor', 'default');
      }
    }); // end mousemove
  };


  GetKeyByName(name) {
    return this.state.filter(e => e.name==name)[0];
  };
  isKeyDown(name) {
    return this.GetKeyByName(name).isDown;
  };


  setFingering(fingering, multiple=false) {
    // TODO: Multiple fingerings
    // Data is an array of 23 booleans
    // representing each key:
    // true=pressed, false: depressed
    // [
    //   false, // 00 LH: octave
    //   false, // 01 LH: frontF
    //   false, // 02 LH: 1
    //   false, // 03 LH: Bb
    //   false, // 04 LH: 2
    //   false, // 05 LH: 3
    //   false, // 06 LH: palm 1
    //   false, // 07 LH: palm 2
    //   false, // 08 LH: palm 3
    //   false, // 09 LH: pinky top
    //   false, // 10 LH: pinky left
    //   false, // 11 LH: pinky right
    //   false, // 12 LH: pinky bottom
    //   false, // 13 RH: 1
    //   false, // 14 RH: 2
    //   false, // 15 RH: 3
    //   false, // 16 RH: side 1
    //   false, // 17 RH: side 2
    //   false, // 18 RH: side 3
    //   false, // 19 RH: highfs
    //   false, // 20 RH: fsAlternate
    //   false, // 21 RH: pinky top
    //   false, // 22 RH: pinky bottom
    // ]
    // This does:
    // - decide if switcher should be on
    // - set fingering to the first fingering
    this.fingering = fingering;
    if (fingering.every((e) => Array.isArray(e))) {
      // multiple fingerings
      this.switcher.enabled = true;
      fingering = fingering[0];
    } else {
      // single fingering
      this.switcher.enabled = false;
    }
    this._setFingering(fingering);
  } // setFindering

  _setFingering(fingering) {
    for (let i in fingering) {
      this.state[i].isDown = false;
      if (fingering[i] == true || fingering[i] == 1) {
        this.state[i].isDown = true;
      }
    }
    this.DrawSax();
  };


  setLabel(label) {
    this.label = label;
    this.isDrawLabel = true;
    this.DrawSax();
  };
  drawLabel() {
    if (this.isDrawLabel) {
      const c = this.context;
      c.font = "30px sans bold";
      //let t = c.measureText(text);
      let pos = this.Transform({x: 74, y: 6});
      c.fillText(this.label, pos.x, pos.y);
    }
  }


  addCanvasEventListener(type, func) {
    // User-provided callback for the canvas
    this.canvas.addEventListener(type, func.bind(this));
  }


  Transform(coords) {
    let a = {...coords};
    a.x = this.unitX*coords.x;
    a.y = this.unitY*coords.y;
    return a;
  }


  ////////////////// DRAWING FUNCTIONS /////////////
  DrawSax() {
    const c = this.context;
    c.clearRect(0, 0, this.canvas.width, this.canvas.height);
    const coords = this.coords;
    // Left hand
    this.DrawOctave();
    this.DrawFrontF(); // above 1
    this.GetKeyByName('lh1').path = this.DrawCircle(coords.lh1, this.isKeyDown('lh1'), null, this.GetKeyByName('lh1').isHighlighted);  // 1
    this.DrawBb(); // Bb, between 1 and 2
    this.GetKeyByName('lh2').path = this.DrawCircle(coords.lh2, this.isKeyDown('lh2'), null, this.GetKeyByName('lh2').isHighlighted);  // 2
    this.GetKeyByName('lh3').path = this.DrawCircle(coords.lh3, this.isKeyDown('lh3'), null, this.GetKeyByName('lh3').isHighlighted);        // 3
    this.DrawPalmKeys();
    this.DrawPinkyLH(coords.lhPinky);

    // Horizontal line
    let a = this.Transform({x: 40, y: 50});
    let b = this.Transform({x: 60, y: 50});
    c.beginPath();
    c.moveTo(a.x, b.y);
    c.lineTo(b.x, b.y);
    c.stroke();

    // Right hand
    this.GetKeyByName('rh1').path = this.DrawCircle(coords.rh1, this.isKeyDown('rh1'), null, this.GetKeyByName('rh1').isHighlighted);    // 1
    this.DrawHighFs(coords.highFs); // between 1 and 2
    this.GetKeyByName('rh2').path = this.DrawCircle(coords.rh2, this.isKeyDown('rh2'), null, this.GetKeyByName('rh2').isHighlighted);    // 2
    this.GetKeyByName('rh3').path = this.DrawCircle(coords.rh3, this.isKeyDown('rh3'), null, this.GetKeyByName('rh3').isHighlighted);    // 3
    this.DrawSideKeys(coords.side);
    this.DrawFsAlternate(coords.fsAlternate); // between 2 and 3
    this.DrawPinkyRH(coords.rhPinky);
    this.drawSwitcher();
    this.drawLabel();
  }; // DrawSax()


  DrawOctave() {
    const c = this.context;
    const co = this.coords.octave;
    const isHighlighted = this.GetKeyByName('octave').isHighlighted;
    const isDown = this.GetKeyByName('octave').isDown;
    const size = (4/3)*this.key_size;
    let path = null;
    path = this.DrawEllipse(co, isDown, this.key_size, isHighlighted);
    this.GetKeyByName('octave').path = path;
  }


  DrawFrontF() {
    const c = this.context;
    const co = this.coords.frontF;
    //const isDown = this.GetKeyByName('frontF').isDown;
    const key = this.GetKeyByName('frontF');
    let size = this.key_size;
    let path = null;
    if (!key.isDown && !this.settings.isStrokeAll) return;
    path = this.DrawSmallCircle(co, key.isDown, key.isHighlighted);
    //let a = this.GetKeyByName('frontF');
    key.path = path;
  } // DrawFrontF


  DrawBb() {
    // Bb: between 1 and 2
    const key = this.GetKeyByName('lhBb');
    const isDown = key.isDown;
    if (!key.isDown && !this.settings.isStrokeAll) {
      return;
    }
    key.path = this.DrawSmallCircle(this.coords.lhBb, key.isDown, key.isHighlighted);
  } // DrawBb


  DrawCircle(coords, isDown, rad=this.key_size, isHighlighted=false) {
    if (rad == null) {
      rad = this.key_size;
    }
    const c = this.context;
    let path = new Path2D();
    //path.arc(d.x, d.y, rad, 0, 2*Math.PI);
    path.arc(coords.x, coords.y, rad, 0, 2*Math.PI);
    if (isHighlighted) {
      c.lineWidth = 5;
    }
    c.stroke(path);
    c.lineWidth = 3;
    if (isDown) c.fill(path);
    return path;
  }
  DrawSmallCircle(coords, isDown, isHighlighted) {
    const c = this.context;
    let path = new Path2D();
    path.arc(coords.x, coords.y, (3/5)*this.key_size, 0, 2*Math.PI);
    if (isHighlighted) {
      c.lineWidth = 5;
    }
    c.stroke(path);
    c.lineWidth = 3;
    if (isDown) c.fill(path);
    return path;
  }


  DrawPalmKeys() {
    const c = this.context;
    const co = this.coords;
    const isDown = [
      this.GetKeyByName('palm1').isDown,
      this.GetKeyByName('palm2').isDown,
      this.GetKeyByName('palm3').isDown,
    ];
    const isHighlighted = [
      this.GetKeyByName('palm1').isHighlighted,
      this.GetKeyByName('palm2').isHighlighted,
      this.GetKeyByName('palm3').isHighlighted,
    ];
    if (!isDown.includes(true) && !this.settings.isStrokeAll) {
      // Do not draw keys unless one of them is pressed
      // or stroking all keys is enforced
      return;
    }
    this.GetKeyByName('palm1').path = this.DrawPalmKey(co.palm1, isDown[0], isHighlighted[0]);
    this.GetKeyByName('palm2').path = this.DrawPalmKey(co.palm2, isDown[1], isHighlighted[1]);
    this.GetKeyByName('palm3').path = this.DrawPalmKey(co.palm3, isDown[2], isHighlighted[2]);
  }
  DrawPalmKey(coords, isDown, isHighlighted) {
    // Cubic Bézier curve
    const c = this.context;
    let path = new Path2D();
    path = this.DrawEllipse(coords, isDown);
    if (isHighlighted) {
      c.lineWidth = 5;
    }
    c.stroke(path);
    c.lineWidth = 3;
    if (isDown) c.fill(path);
    return path;
  } // DrawPalmKeys


  DrawPinkyLH(coords) {
    const c = this.context;
    const x = coords.x;
    const y = coords.y;
    const s = (5/4)*this.key_size;
    const isDown = [
      this.GetKeyByName('lhPinkyTop').isDown,
      this.GetKeyByName('lhPinkyLeft').isDown,
      this.GetKeyByName('lhPinkyRight').isDown,
      this.GetKeyByName('lhPinkyBottom').isDown,
    ];
    const isHighlighted = [
      this.GetKeyByName('lhPinkyTop').isHighlighted,
      this.GetKeyByName('lhPinkyLeft').isHighlighted,
      this.GetKeyByName('lhPinkyRight').isHighlighted,
      this.GetKeyByName('lhPinkyBottom').isHighlighted,
    ];
    if (!isDown.includes(true)
      && !this.settings.isStrokeAll) {
      return;
    }
    // top
    let p = new Path2D;
    p.arc(x, y, s, (-1/6)*Math.PI, (7/6)*Math.PI, true);
    p.moveTo(x-s+2, y-s/2);
    p.lineTo(x+s-2, y-s/2);
    if (isHighlighted[0]) {
      c.lineWidth = 5;
    }
    c.stroke(p);
    c.lineWidth = 3;
    if (isDown[0]) c.fill(p);
    this.GetKeyByName('lhPinkyTop').path = p;

    // left
    p = new Path2D();
    p.arc(x, y, s, (5/6)*Math.PI, (7/6)*Math.PI, false);
    p.lineTo(x, y-s/2);
    p.lineTo(x, y+s/2);
    p.lineTo(x-s+s/10, y+s/2);
    if (isHighlighted[1]) {
      c.lineWidth = 5;
    }
    c.stroke(p);
    c.lineWidth = 3;
    if (isDown[1]) c.fill(p);
    this.GetKeyByName('lhPinkyLeft').path = p;

    // right
    p = new Path2D();
    p.arc(x, y, s, (1/6)*Math.PI, (11/6)*Math.PI, true);
    p.lineTo(x, y-s/2);
    p.lineTo(x, y+s/2);
    p.lineTo(x+s-s/10, y+s/2);
    if (isHighlighted[2]) {
      c.lineWidth = 5;
    }
    c.stroke(p);
    c.lineWidth = 3;
    if (isDown[2]) c.fill(p);
    this.GetKeyByName('lhPinkyRight').path = p;

    // bottom
    p = new Path2D();
    p.arc(x, y, s, (1/6)*Math.PI, (5/6)*Math.PI, false);
    if (isHighlighted[3]) {
      c.lineWidth = 5;
    }
    c.stroke(p);
    c.lineWidth = 3;
    if (isDown[3]) c.fill(p);
    this.GetKeyByName('lhPinkyBottom').path = p;
  } // DrawPinkyLH


  //////////////// RH //////////////
  DrawSideKeys(coords) {
    const c = this.context;
    const x = coords.x;
    const y = coords.y;
    const isDown = [
      this.GetKeyByName('side1').isDown,
      this.GetKeyByName('side2').isDown,
      this.GetKeyByName('side3').isDown,
    ];
    const isHighlighted = [
      this.GetKeyByName('side1').isHighlighted,
      this.GetKeyByName('side2').isHighlighted,
      this.GetKeyByName('side3').isHighlighted,
    ];
    if (!isDown.includes(true) && !this.settings.isStrokeAll) {
      // Do not draw keys unless one of them is pressed
      // or stroking all keys is enforced
      return;
    }
    this.GetKeyByName('side1').path = this.DrawEllipse(coords, isDown[0], this.key_size, isHighlighted[0]);
    this.GetKeyByName('side2').path = this.DrawEllipse({
      x: x,
      y: y+this.Transform({x:0,y:12}).y,
    }, isDown[1], this.key_size, isHighlighted[1]);
    this.GetKeyByName('side3').path = this.DrawEllipse({
      x: x,
      y: y+this.Transform({x:0,y:24}).y,
    }, isDown[2], this.key_size, isHighlighted[2]);
  } // DrawSideKeys


  DrawPinkyRH(coords) {
    const c = this.context;
    const s = (5/4)*this.key_size;
    const isDown = [
      this.GetKeyByName('rhPinky1').isDown,
      this.GetKeyByName('rhPinky2').isDown,
    ];
    const isHighlighted = [
      this.GetKeyByName('rhPinky1').isHighlighted,
      this.GetKeyByName('rhPinky2').isHighlighted,
    ];
    this.DrawCircle(coords, false, s); // contour
    // top
    let x = coords.x;
    let y = coords.y;
    let p = new Path2D();
    p.arc(x, y, s, 0, Math.PI, true);
    p.moveTo(x-s+2, y);
    p.lineTo(x+s-2, y);
    if (isHighlighted[0]) {
      c.lineWidth = 5;
    }
    c.stroke(p);
    c.lineWidth = 3;
    if (isDown[0]) c.fill(p);
    this.GetKeyByName('rhPinky1').path = p;

    // bottom
    p = new Path2D();
    p.arc(x, y, s, 0, Math.PI);
    p.moveTo(x-s+2, y);
    p.lineTo(x+s-3, y);
    if (isHighlighted[1]) {
      c.lineWidth = 5;
    }
    c.stroke(p);
    c.lineWidth = 3;
    if (isDown[1]) c.fill(p);
    this.GetKeyByName('rhPinky2').path = p;
  } // DrawPinkyRH


  DrawHighFs(coords) {
    const c = this.context;
    const isDown = this.GetKeyByName('highFs').isDown;
    const isHighlighted = this.GetKeyByName('highFs').isHighlighted;
    const x = coords.x;
    const y = coords.y;
    if (!isDown && !this.settings.isStrokeAll) {
      // Do not draw keys unless one of them is pressed
      // or stroking all keys is enforced
      return;
    }
    this.GetKeyByName('highFs').path = this.DrawSmallCircle(coords, isDown, isHighlighted);
  }


  DrawFsAlternate(coords) {
    const c = this.context;
    const isDown = this.GetKeyByName('fsAlternate').isDown;
    const isHighlighted = this.GetKeyByName('fsAlternate').isHighlighted;
    const size = this.key_size;
    const x = coords.x;
    const y = coords.y;
    if (!isDown && !this.settings.isStrokeAll) {
      // Do not draw keys unless one of them is pressed
      // or stroking all keys is enforced
      return;
    }
    this.GetKeyByName('fsAlternate').path = this.DrawSmallCircle(coords, isDown, isHighlighted);
  }


  DrawEllipse(coords, isDown, size=this.key_size, isHighlighted=false) {
    const c = this.context;
    let path = new Path2D();
    path.ellipse(
      coords.x, coords.y,
      (40/100)*size, // radX
      size,          // radY
      0, 0, 2*Math.PI);
    if (isHighlighted) {
      c.lineWidth = 5;
    }
    c.stroke(path);
    c.lineWidth = 3;
    if (isDown) c.fill(path);
    return path;
  }
}; // class Sax
