window.onload = function() {
  const sax = new Sax('canvas', {
    isStrokeAll: true,
  });

  sax.addCanvasEventListener('mousedown', function(e) {
    // Open or close pads on click
    const c = this.context;
    const state = this.state;
    // Mouse position relative to canvas
    var rect = this.canvas.getBoundingClientRect();
    const mouseX = e.clientX - rect.left;
    const mouseY = e.clientY - rect.top;
    for (i in state) {
      let s = document.getElementById('key-' + i);
      if (state[i].hasOwnProperty('path')) {
        if (c.isPointInPath(state[i].path, mouseX, mouseY)) {
          state[i].isDown = !state[i].isDown;
          this.DrawSax();
          let s = document.getElementById('key-' + i);
          if (s.innerHTML == '1') {
            s.innerHTML = '0';
          } else {
            s.innerHTML = '1';
          }
        }
      }
    }
  });

  sax.addCanvasEventListener('mousemove', function(e) {
    // Highlight pads on hover
    const c = this.context;
    const state = this.state;
    // Mouse position relative to canvas
    var rect = this.canvas.getBoundingClientRect();
    const mouseX = e.clientX - rect.left;
    const mouseY = e.clientY - rect.top;
    for (i in state) {
      state[i].isHighlighted = false;
      let s = document.getElementById('key-' + i);
      s.style = 'border: 1px solid white';
      if (state[i].hasOwnProperty('path')) {
        if (c.isPointInPath(state[i].path, mouseX, mouseY)) {
          state[i].isHighlighted = true;
          s.style = 'border: 1px solid black; font-weight:bold; text-align:center;';
        }
      }
    }
    sax.DrawSax();
  });

  const layout = [ 2, 4, 3, 4, 'space', 3, 3, 2, 2 ];
  const root = document.getElementById('keys');
  let k = 0;
  for (let i of layout) {
    if (Number.isInteger(i)) {
      for (let j = 1; j<=i; j++) {
        let s = document.createElement('span');
        s.style = 'border: 1px solid white';
        s.innerHTML = '0';
        s.id = 'key-' + k;
        root.appendChild(s);
        k++;
      }
      let spacer = document.createElement('span');
      spacer.innerHTML = '&nbsp;';
      root.appendChild(spacer);
    } else {
      let bigSpacer = document.createElement('span');
      bigSpacer.innerHTML = '&nbsp;&nbsp;';
      root.appendChild(bigSpacer);
    }
  }
}
