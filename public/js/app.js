let settings = {
  isBreakOnOctave: true,
  isShowOnlyScale: true,
  size: 5,
};
let cards = [];
function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}
const noteNames = [
    'C', 'C#', 'D', 'D#', 'E', 'F',
    'F#', 'G', 'G#', 'A', 'A#', 'B'
];
const COUNT_SAX_NOTES = 32;
const INTERVALS = {
  maj: [ 0, 2, 4, 5, 7, 9, 11 ],
  min: [ 0, 2, 3, 5, 7, 8, 10 ],
};
let SAX_NOTE_NAMES = [];
let btnSettings = null;
let viewSettings = null;
let btnSettingsClose = null;
let synth = new Tinysynth();


window.onload = function() {
  // Main app
  drawPicker();
  SAX_NOTE_NAMES = saxNoteNames();
  drawCards();

  // Buttons and overlays
  btnSettings = _getId('settings');
  btnSettings.addEventListener('click', openSettings);
  btnSettingsClose = _getId('btn-settings-close');
  btnSettingsClose.addEventListener('click', openSettings);
  viewSettings = _getId('view-settings');
  _getId('octave-newline').addEventListener('change', onOctaveNewlineUpdate);
  _getId('scale-notes-only').addEventListener('change', onScaleNotesOnly);
}


function drawCards() {
  // Clear
  const div = _getId('app');
  div.innerHTML = '';
  for (card in cards) {
    delete cards[card];
  }
  cards = [];

  // Get scale key and scale type from inputs
  let scaleKey = _getId('select_key_note').value;
  let scaleType = _getId('select_key_type').value;

  // Draw cards
  let isFirstLine = true;
  let scaleNoteNames = getScaleNoteNames(scaleKey, scaleType);
  for (let i = 0; i < SAX_NOTE_NAMES.length; i++) {
    // For each note available on sax
    let noteName = SAX_NOTE_NAMES[i];
    if (settings.isShowOnlyScale) {
      if (!scaleNoteNames.includes(noteName)) {
        // Skip notes which are not in the scale
        continue;
      }
    }

    if (isFirstLine) {
      // Reached the first note of our scale here
      for (let i = 0; i < scaleNoteNames.indexOf(noteName); i++) {
        // Pad the first line if needed
        div.appendChild(_new('div'));
      }
      isFirstLine = false;
    }

    // Draw card
    const canvasId = 'canvas' + i;
    let canvas = _new('canvas');
    canvas.id = canvasId;
    canvas.className = 'card';
    canvas.dataset.freq = getFreqFromMidi(49+i);
    div.appendChild(canvas);
    let card = new Sax(canvasId);
    card.setFingering(FINGERINGS[i]);
    card.setLabel(noteName);
    cards.push(card);
    canvas.addEventListener('mousedown', onCardClick);
  }
}


function getScaleNoteNames(keyNote, scaleType) {
  // if keyNote=C#, scaleType=maj
  // return: C#, D#, F, F#, G#, A#, C
  let shiftedNoteNames = noteNames;
  let intervals = (scaleType == 'maj')? INTERVALS.maj : INTERVALS.min;
  let shift = shiftedNoteNames.indexOf(keyNote);
  for (let i = 0; i<shift; i++) {
    let first = shiftedNoteNames.shift();
    shiftedNoteNames.push(first);
  }
  let scaleNoteNames = [];
  for (let i of intervals) {
    scaleNoteNames.push(shiftedNoteNames[i]);
  }
  return scaleNoteNames;
}


function getNoteAt(i) {
  // return a note name:
  // 0=A#, 1=B, 2=C, ..., 12=A#,
  // 13=B, 14=C
  return noteNames[(i+10)%12];
}


function saxNoteNames() {
  // return names of all sax notes:
  // A#, B, C, ..., F
  let notes = [];
  for (let i=0; i<=COUNT_SAX_NOTES-1; i++) {
    notes.push(getNoteAt(i));
  }
  return notes;
}


function onCardClick(e) {
  let note = parseFloat(e.target.dataset.freq);
  synth.noteOn(note, duration=500);
}

function drawPicker() {
  // GUI picker:
  // - key note (C, C#, ...)
  // - type: maj/min
  const div = _getId('controls');
  let selectNote = _new('select');
  div.appendChild(selectNote);
  selectNote.id = 'select_key_note';
  for (const note of noteNames) {
    let opt = _new('option');
    opt.innerHTML = note;
    selectNote.appendChild(opt);
  }
  let sType = _new('select');
  div.appendChild(sType);
  sType.id = 'select_key_type';
  for (const type of ['maj', 'min']) {
    let opt = _new('option');
    opt.innerHTML = type;
    sType.appendChild(opt);
  }
  selectNote.addEventListener('change', drawCards);
  sType.addEventListener('change', drawCards);
}


function openSettings() {
  // Show/hide settings overlay
  let visibility = viewSettings.style.getPropertyValue('visibility');
  let newStyle = 'hidden';
  if (visibility == '' || visibility == 'hidden') {
    newStyle = 'visible';
  }
  viewSettings.style.setProperty('visibility', newStyle);
}

// ref: https://www.music.mcgill.ca/~gary/307/week1/node28.html
function getFreqFromMidi(midiNote) {
  // 440*2^((n-69)/12)
  let n = midiNote;
  return 440*Math.pow(2, (n-69)/12);
}


///// SETTINGS ////
function onOctaveNewlineUpdate(e) {
  settings.isBreakOnOctave = e.target.checked;
  drawCards();
}
function onScaleNotesOnly(e) {
  settings.isShowOnlyScale = e.target.checked;
  let app = _getId('app');
  if (e.target.checked) {
    app.style.setProperty('grid-template-columns', 'repeat(7, 1fr)');
  } else {
    app.style.setProperty('grid-template-columns', 'repeat(18, 1fr)');
  }
  drawCards();
}
